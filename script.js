let targetShooted = 0;
let bullets = 10;
let stringBullet;

let msec = 0,
    sec = 0,
    min = 1,
    timerOn = 0;
    let msecVariable;

    window.onload

function setMSec() {
    if (msec < 10) {
        document.getElementById("timer__msec").innerHTML = "0" + msec;
    } else {
        document.getElementById("timer__msec").innerHTML = msec;
    }
    msec = msec + 1;
    msecVariable = setTimeout(setMSec, 100);
    if (msec >= 10) {
        msec = 0;
        sec +=1;
        setSec();
  }
}

function setSec() {
    if (sec < 10) {
        document.getElementById("timer__sec").innerHTML = "0" + sec;
      } 
    else if (sec >= 60) {
        min +=1;
        alert("Llegué");
    }
    else {
        document.getElementById("timer__sec").innerHTML = sec;
      }
    
}

function startTimer() {
    if (!timerOn) {
        timerOn = 1;
        setMSec();
    }
}

function stopTimer() {
    clearTimeout(msecVariable);
    timerOn = 0;

    if (msec == 0) {
        msec = 9
    }
    else {
        msec = msec - 1;
    }

    if (msec < 10) {
        document.getElementById("mark__msec").innerHTML = "0" + msec;
    } else {
        document.getElementById("mark__msec").innerHTML = msec;
    }

    if (sec < 10) {
        document.getElementById("mark__sec").innerHTML = "0" + sec;
    } else {
        document.getElementById("mark__sec").innerHTML = sec;
    }
    reset();
}

function getRandomX() {
    return Math.random() * (748 - 3) + 3
}

function getRandomY() {
    return Math.random() * (448 - 3) + 3
}

function playShoot() {
    let audioShoot = document.getElementById("audioShoot");
    audioShoot.play();
}

function playReload() {
    let audioReload = document.getElementById("audioReload");
    audioReload.play();
}

function start() {
    startTimer();

    if (bullets == 0){
        reload();
    }

    playReload();
    document.getElementById("game__button").style.display = "none";
    generate();
}

function generate() {


    let X = getRandomX();
    let Y = getRandomY();

    let getTarget = document.getElementById("target");
    let existTarget = document.getElementById("game__image").contains(getTarget);


    if (existTarget === true) {
        getTarget.remove();
        targetShooted +=1;
        stringBullet = "bullet" + bullets;
        document.getElementById(stringBullet).style.display = "none";
        bullets -=1;
    }

    if (targetShooted === 10) {
        stopTimer();
    }
    else {
        let target = document.createElement("target");
        target.style.position = "relative";

        target.style.left = X + "px";
        target.style.top = Y + "px";

        target.className = "target";
        target.id = "target";
        target.onclick = function() {
            playShoot();
            generate();
        };
        
        document.getElementById("game__image").appendChild(target);
    }
    
}

function reset() {
    msec = 0;
    sec = 0;
    msecVariable = 0;
    document.getElementById("timer__msec").innerHTML = "0" + msec;
    document.getElementById("timer__sec").innerHTML = "0" + msec;
    
    targetShooted = 0;
    document.getElementById("game__button").style.display = "block";
}

function reload() {
    for (let i = 1; i <= 10; i ++) {
        stringBullet = "bullet" + i; 
        document.getElementById(stringBullet).style.display = "block";
    }
    bullets = 10;
}